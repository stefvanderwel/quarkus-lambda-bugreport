package nl.stefvanderwel;

import io.quarkus.test.junit.QuarkusTest;
import io.restassured.RestAssured;
import org.junit.jupiter.api.Test;

import static org.hamcrest.Matchers.equalTo;

@QuarkusTest
public class GreetingTest
{
    @Test
    public void testJaxrs() {
        RestAssured.when()
                .get("/hello/auth0|6e27543b74dfe10eaa478c95")
                .then()
                .contentType("text/plain")
                .body(equalTo("hello auth0|6e27543b74dfe10eaa478c95"));
    }

    @Test
    public void testServlet() {
        RestAssured.when().get("/servlet/hello").then()
                .contentType("text/plain")
                .body(equalTo("hello servlet"));
    }

    @Test
    public void testVertx() {
        RestAssured.when().get("/vertx/hello/auth0|6e27543b74dfe10eaa478c95").then()
                .contentType("text/plain")
                .body(equalTo("hello auth0|6e27543b74dfe10eaa478c95"));
    }

    @Test
    public void testFunqy() {
        RestAssured.when().get("/funqyHello").then()
                .contentType("application/json")
                .body(equalTo("\"hello funqy\""));
    }

}
